# Schultzcre

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

---

## Package Structure

## src/app
Contains all application components, modules, routing and imports. 

---

> #### src/app -> about, contact, home, our-work
> Contains page specific components, styles, and markup. These folder names bind to the pages they render. They may import shared various components from other folders.
> - About page renders the `about.component.ts` angular component
> - Contact page renders the `contact.component.ts` angular component
> - Home page renders the `home.component.ts` angular component
> - Our Work page renders the `our-work.component.ts` page
> All of these routes are mapped in the `app.module.ts` angular module

> #### src/app/admin
> Contains the separated lazy loaded `admin.module.ts` module to render child routes under the `/admin` route. These routes are not accessible through any navigation in the client application besides going directly to them in the address bar.
> - `/maps`: Contains the map entries page for editing the locations loaded into the site. All of these values are stored in Firebase Firestore. The client application is subscribing to these collections so the front end will automatically load new data.

> #### src/app/services
> Contains service classes to communicate between external APIs, Firebase Firestore data, or for providing `@Injectable` services to components for sharing data.

## functions
Contains serverless functions deployed to Firebase Cloud Functions

---

> #### src/index.ts
> Contains the serverless functions configuration and code executor for any functions. Currently there is one function that will auto run whenever the contact form is filled out and a record is added to the DB.

## Environment Information

This project was built using Angular v5.x and Node v8.x. You will need to have a shell using node@8 to install dependencies. Both the root and the functions folder contains separated `package.json` files therefore will need separate npm installs. The build output for the functions are ignored from git as that should be built during the time of deploy so no stale code gets deployed to the serverless functions. 

### Tooling
> - Node v8.x
> - Angular v5.x
> - Angular CLI v5.x
> - Firebase CLI

