import * as functions from 'firebase-functions';

import * as sgmail from '@sendgrid/mail';
sgmail.setApiKey(functions.config().sendgrid.key);

const templateId = 'd-2c16bb7d0f984325a030d60f0baa5193';

exports.triggerEmail = functions.firestore
    .document('contactRequests/{userId}')
    .onCreate(async (event) => {
        const data = event.data();
        const msg = {
            to: 'erich@schultzcre.com',
            from: 'npesa1992@gmail.com',
            subject: 'New Form Submission',
            templateId,
            substitutionWrappers: ['{{', '}}'],
            substitutions: {
                name: data.name,
                phone: data.phoneNumber,
                email: data.email
            },
        };
        await sgmail.send(msg);
        console.log('New Submission: ', data);
        return 'ok';
    });