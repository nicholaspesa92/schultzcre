import { Component, OnInit } from '@angular/core';

import { UrlSanitizerService } from './../ng-services/url-sanitizer.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  logo: any;

  first: any;
  second: any;

  iconRoot: string;
  services: any[];

  erichSrc: any;

  constructor(
    private urlS: UrlSanitizerService
  ) {
    this.iconRoot = './../../assets/icons';
    this.logo = './../../assets/images/logo_white.png';
    this.erichSrc = this.urlS.getSanitizerStyle('./../../assets/images/erich_coat.jpg');
  }

  ngOnInit() {
    this.setFirstContent();
    this.setSecondContent();
    this.buildServices();
  }

  setFirstContent() {
    this.first = {
      tag: "Real estate is Erich's passion.",
      body: `Erich is a lifer having dedicated all 32 years of his professional career to commercial real estate.After college, he was immediately hired by Coldwell Banker Commercial Real Estate Services (now CBRE) in Denver.He later went to work in the Torrance office of The Seeley Company (now Colliers International) focused on the South Bay area of Southern California.In 1993, Erich and three partners started Compass Commercial Real Estate Services, a full service real estate company based in Bend, Oregon.Today, Compass Commercial is the largest commercial brokerage and property management company in Central and Eastern Oregon.Erich was first or second in production every year, and has successfully completed more than 1,000 lease transactions and 300 property sales.At the end of 2017, he left the company he co-founded to start Schultz CRE`
    };
  }

  setSecondContent() {
    this.second = {
      tag: 'Qualified, knowledgeable and experienced.',
      body: `In 1995, became the first Central Oregon member of the Society of Industrial and Office Realtors (SIOR), the leading global professional office and industrial real estate association.To qualify, brokers must meet stringent experiential, education and ethical requirements.Real estate professionals who have earned the SIOR designation are recognized by corporate real estate executives, commercial real estate brokers, agents, lenders,and other real estate professionals as the most capable and experienced brokerage practitioners in any market.Today, SIOR members are found in 680 cities in 34 countries, allowing members to team up on assignments well beyond their home base.`
    };
  }

  buildServices() {
    this.services = [
      {
        icon: this.sanitizeImage(`${this.iconRoot}/light_bulb_beige.png`),
        title: 'Consulting',
        types: [
          'Market Research & Analysis',
          'Feasibility Studies',
          'Broker Opinions of Value',
          'Due Diligence'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/house_navy.png`),
        title: 'Development',
        types: [
          'Built to Suit',
          'Repositioning',
          'Partnering'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/money_beige.png`),
        title: 'Investments',
        types: [
          'Local, Regional, and National Searches and Evaluation',
          'Tax Deferred Exchanges'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/house_burgundy.png`),
        title: 'Brokerage',
        types: [
          'Tenant Representation',
          'Buyer Representation',
          'Investment Sales'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      }
    ];
  }

  sanitizeImage(url) {
    return this.urlS.getSanitizedImageResource(url);
  }
}
