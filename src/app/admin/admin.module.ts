import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminComponent } from './admin.component';
import { MapsComponent } from './maps/maps.component';
import { AddPlaceComponent } from './maps/add-place/add-place.component';
import { EditPlaceComponent } from './maps/edit-place/edit-place.component';
import { MessagesComponent } from './messages/messages.component';

import { MapService } from './../ng-services/map.service';

import { environment } from './../../environments/environment';

import {
  MatTabsModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatSortModule,
  MatDialogModule,
  MatSelectModule
} from '@angular/material';

import { MatTableModule } from '@angular/material/table';

const mat = [
  MatTabsModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatTableModule,
  MatSortModule,
  MatDialogModule,
  MatSelectModule
];

const routes: Routes = [
  { path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'maps', pathMatch: 'full' },
      {
        path: 'maps',
        component: MapsComponent,
        data: {
          home: false,
          show: false,
          footer: true
        }
      },
      {
        path: 'maps/add-place',
        component: AddPlaceComponent,
        data: {
          home: false,
          show: false,
          footer: true
        }
      },
      {
        path: 'messages',
        component: MessagesComponent,
        data: {
          home: false,
          show: false,
          footer: true
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    mat,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AgmCoreModule.forRoot({
      apiKey: environment.firebase.apiKey,
      libraries: ['places']
    }),
    ReactiveFormsModule
  ],
  declarations: [
    AdminComponent,
    MapsComponent,
    AddPlaceComponent,
    EditPlaceComponent,
    MessagesComponent
  ],
  providers: [
    MapService
  ],
  entryComponents: [
    EditPlaceComponent
  ]
})
export class AdminModule { }
