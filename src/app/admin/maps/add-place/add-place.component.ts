import { Component, OnInit, Inject, NgZone, ViewChild, ElementRef } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import * as firebase from 'firebase';

import { MapService } from './../../../ng-services/map.service';

@Component({
  selector: 'app-add-place',
  templateUrl: './add-place.component.html',
  styleUrls: ['./add-place.component.scss']
})
export class AddPlaceComponent implements OnInit {

  placeGroup: FormGroup;

  lat: any;
  lon: any;

  categories: any[];
  subcategories: any[];

  @ViewChild('search') public searchElementRef: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<AddPlaceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private mapsLoader: MapsAPILoader,
    private ngZone: NgZone,
    private ms: MapService
  ) {
    this.lat = 0;
    this.lon = 0;
  }

  ngOnInit() {
    this.setCategorizations();
    this.buildForm();
    this.mapsLoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          console.log(place);

          this.lat = place.geometry.location.lat();
          this.lon = place.geometry.location.lng();
          const coordinates = new firebase.firestore.GeoPoint(this.lat, this.lon);

          const location = {
            address: {
              streetNumber: '',
              street: '',
              number: '',
            },
            city: '',
            state: '',
            zip: '',
            country: ''
          };
          place.address_components.forEach(comp => {
            if (comp.types.indexOf('street_number') >= 0) {
              location.address.streetNumber = comp.short_name;
            }
            if (comp.types.indexOf('route') >= 0) {
              location.address.street = comp.short_name;
            }
            if (comp.types.indexOf('subpremise') >= 0) {
              location.address.number = comp.short_name;
            }
            if (comp.types.indexOf('locality') >= 0) {
              location.city = comp.short_name;
            }
            if (comp.types.indexOf('administrative_area_level_1') >= 0) {
              location.state = comp.short_name;
            }
            if (comp.types.indexOf('postal_code') >= 0) {
              location.zip = comp.short_name;
            }
            if (comp.types.indexOf('country') >= 0) {
              location.country = comp.short_name;
            }
          });

          this.placeGroup.patchValue({
            placeName: place.name,
            location: coordinates,
            googleLocationId: place.id,
            googlePlaceId: place.place_id,
            phoneNumber: place.formatted_phone_number,
            address: location.address.streetNumber + ' ' + location.address.street,
            address2: location.address.number,
            city: location.city,
            state: location.state,
            zip: location.zip,
            photos: place.photos
          });
        });
      });
    });
  }

  buildForm() {
    this.placeGroup = this.fb.group({
      placeName: ['', Validators.required],
      photoUrl: '',
      location: '',
      googleLocationId: '',
      googlePlaceId: '',
      phoneNumber: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      address2: '',
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required],
      photos: '',
      description: '',
      category: ['', Validators.required],
      subcategory: ['', Validators.required]
    });
  }

  clear() {
    this.lat = 0;
    this.lon = 0;
    this.placeGroup.patchValue({
      placeName: '',
      location: '',
      googleLocationId: '',
      googlePlaceId: '',
      phoneNumber: '',
      address: '',
      address2: '',
      city: '',
      state: '',
      zip: '',
      photos: '',
      description: '',
      category: '',
      subcategory: ''
    });
  }

  save() {
    const x = this.placeGroup.value;
    const data = {
      placeName: x.placeName ? x.placeName : '',
      address: {
        street: x.address ? x.address : '',
        city: x.city ? x.city : '',
        state: x.state ? x.state : '',
        zip: x.zip ? x.zip : '',
      },
      location: x.location ? x.location : '',
      phoneNumber: x.phoneNumber ? x.phoneNumber : '',
      // photos: x.photos ? x.photos : '',
      googleLocationId: x.googleLocationId ? x.googleLocationId : '',
      googlePlaceId: x.googlePlaceId ? x.googlePlaceId : '',
      category: x.category,
      subcategory: x.subcategory
    };
    this.ms.savePlace(data)
      .then((res) => {
        console.log('success');
        this.dialogRef.close();
      }).catch((error) => {
        console.log(error);
      });
  }

  setCategorizations() {
    this.categories = [
      'Development for own account',
      'Build to suits',
      'Ground up lease listings',
      'Investment Sales/Exchanges',
      'Lease listings',
      'Subdivision Lot Sales',
      'Tenant Representation',
      'Receiver Assignments'
    ];
    this.subcategories = [
      'office',
      'industrial',
      'retail',
      'mixed use',
      'multi-family residential',
      'other'
    ];
  }

  null(val) {
  }

}
