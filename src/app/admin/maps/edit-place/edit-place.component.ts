import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { MapService } from './../../../ng-services/map.service';

@Component({
  selector: 'app-edit-place',
  templateUrl: './edit-place.component.html',
  styleUrls: ['./edit-place.component.scss']
})
export class EditPlaceComponent implements OnInit {

  placeGroup: FormGroup;
  placeId: string;

  categories: any[];
  subcategories: any[];

  constructor(
    private ms: MapService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditPlaceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.placeId = this.data.id;
    this.setCategorizations();
    this.buildForm();
    this.patchForm();
  }

  patchForm() {
    this.placeGroup.patchValue(this.data);
  }

  buildForm() {
    this.placeGroup = this.fb.group({
      placeName: ['', Validators.required],
      phoneNumber: '',
      street: '',
      city: '',
      state: '',
      zip: '',
      description: '',
      category: '',
      subcategory: ''
    });
  }

  save() {
    this.placeGroup.value.description = this.placeGroup.value.description.replace(/\n/g, '<br>');
    console.log(this.placeGroup.value.description);
    this.ms.updatePlace(this.placeId, this.placeGroup.value)
      .then((res) => {
        console.log('success');
        this.dialogRef.close();
      }).catch((error) => {
        console.log(error);
      });
  }

  delete() {
    this.ms.deletePlace(this.placeId)
      .then((res) => {
        console.log('deleted');
        this.dialogRef.close();
      }).catch((error) => {
        console.log(error);
      });
  }

  setCategorizations() {
    this.categories = [
      'Development for own account',
      'Build to suits',
      'Ground up lease listings',
      'Investment Sales/Exchanges',
      'Lease listings',
      'Subdivision Lot Sales',
      'Tenant Representation',
      'Receiver Assignments'
    ];
    this.subcategories = [
      'office',
      'industrial',
      'retail',
      'mixed use',
      'multi-family residential',
      'other'
    ];
  }

}
