import { Component, OnInit, ViewChild, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatSort, MatDialog } from '@angular/material';

import { MapService } from './../../ng-services/map.service';
import { Place } from './../../models/place';

import { AddPlaceComponent } from './add-place/add-place.component';
import { EditPlaceComponent } from './edit-place/edit-place.component';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, AfterViewInit {

  places$: Observable<Place[]>;
  places: Place[];

  dataSource;
  displayColumns = ['position', 'placeName', 'address', 'description'];

  @ViewChild(MatSort) matSort: MatSort;

  constructor(
    private ms: MapService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.places$ = this.ms.getPlaces();
    this.places$.subscribe((places) => {
      this.places = places;
      for (let i = 0; i < this.places.length; i++) {
        this.places[i].no = i + 1;
      }
      console.log(this.places);
      this.dataSource = new MatTableDataSource(this.places);
      this.dataSource.sort = this.matSort;
    });
  }

  add() {
    const config = { width: '540px' };
    const dRef = this.dialog.open(AddPlaceComponent, config);
  }

  edit(place) {
    console.log(place);
    const x = {
      placeName: place.placeName,
      phoneNumber: place.phoneNumber ? place.phoneNumber : '',
      street: place.address.street,
      city: place.address.city,
      state: place.address.city,
      zip: place.address.zip,
      description: place.description ? place.description : '',
      location: {
        latitude: place.location.latitude,
        longitude: place.location.longitude
      },
      id: place.id,
      category: place.category,
      subcategory: place.subcategory
    };
    const config = {
      width: '540px',
      data: x
    };
    const dRef = this.dialog.open(EditPlaceComponent, config);
  }

  /*save() {
    const x = this.placeGroup.value;
    const data = {
      placeName: x.placeName ? x.placeName : '',
      address: {
        street: x.address ? x.address : '',
        city: x.city ? x.city : '',
        state: x.state ? x.state : '',
        zip: x.zip ? x.zip : '',
      },
      location: x.location ? x.location : '',
      phoneNumber: x.phoneNumber ? x.phoneNumber : '',
      photos: x.photos ? x.photos : '',
      googleLocationId: x.googleLocationId ? x.googleLocationId : '',
      googlePlaceId: x.googlePlaceId ? x.googlePlaceId : ''
    }
    this.ms.savePlace(data)
      .then((res) => {
        console.log('success');
      }).catch((error) => {
        console.log(error);
      });
  }*/

}
