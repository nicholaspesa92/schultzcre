import { Component, OnInit, HostListener, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CdkScrollable } from '@angular/cdk/scrolling';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

import { UrlSanitizerService } from './ng-services/url-sanitizer.service';
import { ToolbarService } from './ng-services/toolbar.service';

import { Reference } from '@firebase/storage-types';

const SMALL_WIDTH_BREAKPOINT = 960;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild(CdkScrollable) appContent: CdkScrollable;

  title = 'app';

  tabs: any[];

  logo: any;
  whiteLogo: any;
  blueLogo: any;

  colored: boolean;
  home: boolean;

  toolbarObs: Observable<boolean>;
  toolbarFlag: boolean;

  footer: boolean;

  @HostListener('window:scroll', ['$event']) track(event) { this.trackScroll(window.pageYOffset); }

  constructor(
    private urlS: UrlSanitizerService,
    private router: Router,
    private aRoute: ActivatedRoute,
    private ts: ToolbarService
  ) {
    this.whiteLogo = urlS.getSanitizedImageResource('./../assets/images/logo_white.png');
    this.blueLogo = urlS.getSanitizedImageResource('./../assets/images/logo_blue.png');
    this.logo = urlS.getSanitizedImageResource(this.whiteLogo);
    this.colored = false;
    this.footer = true;
  }

  ngOnInit() {
    this.buildTabs();
    this.toolbarFlag = true;

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.aRoute)
      .map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      })
      .mergeMap(route => route.data)
      .subscribe((event: any) => {
        this.home = event.home;
        this.toolbarFlag = event.show;
        this.footer = event.footer;
        window.scrollTo(0, 0);
        console.log(event);
      });
  }

  ngAfterViewInit() {
    if (this.appContent) {
      this.appContent.elementScrolled().subscribe((scroll) => {
        const x = this.appContent.getElementRef().nativeElement.scrollTop;
        if (this.home) {
          x > 100 ? this.greaterScroll() : this.lesserScroll();
          console.log('test', x);
        }
      });
    }
  }

  buildTabs() {
    this.tabs = [
      { name: 'Home', link: 'home' },
      { name: 'About', link: 'about' },
      { name: 'Work History', link: 'work-history' },
      { name: 'Contact', link: 'contact' }
    ];
  }

  trackScroll(yOff) {
    const x = this.isScreenSmall() ? 129 : 64;
    if (this.home) {
      yOff > 100 ? this.greaterScroll() : this.lesserScroll();
    }
  }

  greaterScroll() {
    console.log('g');
    this.colored = true;
    this.logo = this.urlS.getSanitizedImageResource(this.blueLogo);
  }
  lesserScroll() {
    console.log('l');
    this.colored = false;
    this.logo = this.urlS.getSanitizedImageResource(this.whiteLogo);
  }

  isScreenSmall(): boolean {
    return window.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`).matches;
  }

  sanitizeImage(url) {
    return this.urlS.getSanitizedImageResource(url);
  }
}
