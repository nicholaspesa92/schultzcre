import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { MaterialModule } from './shared/material.module';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OurWorkComponent } from './our-work/our-work.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';


import { UrlSanitizerService } from './ng-services/url-sanitizer.service';
import { ToolbarService } from './ng-services/toolbar.service';
import { MapService } from './ng-services/map.service';
import { ContactService } from './ng-services/contact.service';

import { environment } from './../environments/environment';

const routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    component: HomeComponent,
    data: {
      home: true,
      show: true,
      footer: true
    }
  },
  {
    path: 'about',
    component: AboutComponent,
    data: {
      home: true,
      show: true,
      footer: true
    }
  },
  {
    path: 'work-history',
    component: OurWorkComponent,
    data: {
      home: true,
      show: true,
      footer: true
    }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: {
      home: true,
      show: true,
      footer: false
    }
  },
  { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OurWorkComponent,
    ContactComponent,
    FooterComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    FlexLayoutModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.firebase.apiKey
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [
    UrlSanitizerService,
    ToolbarService,
    MapService,
    ContactService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
