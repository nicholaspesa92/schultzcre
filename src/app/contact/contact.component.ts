import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { UrlSanitizerService } from './../ng-services/url-sanitizer.service';
import { ContactService } from './../ng-services/contact.service';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  logo: string;

  cForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private urlS: UrlSanitizerService,
    private cs: ContactService
  ) {
    this.logo = './../../assets/svg/mail.svg';
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.cForm = this.fb.group({
      name: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_REG_EX)]],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  sanitizeImage(url) {
    return this.urlS.getSanitizedImageResource(url);
  }

  submitForm() {
    const data = this.cForm.value;
    data.createdAt = new Date();
    this.cs.submitForm(data);
  }

}
