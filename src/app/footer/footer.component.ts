import { Component, OnInit } from '@angular/core';

import { UrlSanitizerService } from './../ng-services/url-sanitizer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  logo: any;

  constructor(
    private urlS: UrlSanitizerService
  ) {
    this.logo = urlS.getSanitizedImageResource('./../../assets/images/logo_blue.png');
  }

  ngOnInit() { }

}
