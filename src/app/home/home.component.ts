import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { UrlSanitizerService } from './../ng-services/url-sanitizer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  logo: string;

  iconRoot: string;

  services: any[];
  homeContact: FormGroup;

  constructor(
    private urlS: UrlSanitizerService,
    private fb: FormBuilder
  ) {
    this.iconRoot = './../../assets/icons';
    this.logo = './../../assets/images/logo_white.png';
  }

  ngOnInit() {
    this.buildServices();
    this.buildHomeContact();
  }

  buildHomeContact() {
    this.homeContact = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      phone: ['', [Validators.required]]
    });
  }

  buildServices() {
    this.services = [
      {
        icon: this.sanitizeImage(`${this.iconRoot}/light_bulb_beige.png`),
        title: 'Consulting',
        matIcon: 'trending_up',
        color: 'tan',
        types: [
          'Market Research & Analysis',
          'Feasibility Studies',
          'Broker Opinions of Value',
          'Due Diligence'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/house_navy.png`),
        title: 'Development',
        matIcon: 'build',
        color: 'blue',
        types: [
          'Built to Suit',
          'Repositioning',
          'Partnering'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/money_beige.png`),
        title: 'Investments',
        matIcon: 'local_atm',
        color: 'beige',
        types: [
          'Local, Regional, and National Searches and Evaluation',
          'Tax Deferred Exchanges'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
      {
        icon: this.sanitizeImage(`${this.iconRoot}/house_burgundy.png`),
        title: 'Brokerage',
        matIcon: 'domain',
        color: 'brick',
        types: [
          'Tenant Representation',
          'Buyer Representation',
          'Investment Sales'
        ],
        // tslint:disable-next-line:max-line-length
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      }
    ];
  }

  sanitizeImage(url) {
    return this.urlS.getSanitizedImageResource(url);
  }

}
