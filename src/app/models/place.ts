export interface Place {
    name?: string;
    placeName: string;
    address: any;
    no?: any;
    id?: string;
    location: any;
    phoneNumber: any;
    photos: any;
    googleLocationId: any;
    googlePlaceId: any;
    description: any;
}