import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class ContactService {

  constructor(
    private afs: AngularFirestore
  ) { }

  submitForm(data) {
    return this.afs.collection(`contactRequests`).add(data);
  }
}
