import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import { Place } from './../models/place';

@Injectable()
export class MapService {

  constructor(
    private afs: AngularFirestore
  ) { }

  getPlaces() {
    return this.afs.collection<Place[]>('places').snapshotChanges()
      .map((places) => {
        return places.map((place) => {
          const id = place.payload.doc.id;
          const data = place.payload.doc.data() as Place;
          return { id, ...data };
        });
      });
  }

  getPlace(pId) {
    return this.afs.doc(`places/${pId}`).snapshotChanges()
      .map((place) => {
        const id = place.payload.id;
        const data = place.payload.data();
        return { id, ...data };
      });
  }

  savePlace(data) {
    return this.afs.collection(`places`).add(data);
  }

  updatePlace(id, data) {
    return this.afs.doc(`places/${id}`).update(data);
  }

  deletePlace(id) {
    return this.afs.doc(`places/${id}`).delete();
  }

}
