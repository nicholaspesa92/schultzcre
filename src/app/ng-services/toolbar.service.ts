import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ToolbarService {

  toolbarSub: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  setToolbar(flag) {
    this.toolbarSub.next(flag);
  }

  getToolbarObs() {
    return this.toolbarSub.asObservable();
  }

}
