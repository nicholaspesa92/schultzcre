import { Injectable } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Injectable()
export class UrlSanitizerService {

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  getSanitizedImageResource(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getSanitizedStyleUrl(url) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
  }

  getSanitizerStyle(style) {
    return this.sanitizer.bypassSecurityTrustStyle(style);
  }

}
