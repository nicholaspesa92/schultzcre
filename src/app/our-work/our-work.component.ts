import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { MatSidenav } from '@angular/material';

import { MapService } from './../ng-services/map.service';
import { Place } from './../models/place';

const SMALL_WIDTH_BREAKPOINT = 1024;

@Component({
  selector: 'app-our-work',
  templateUrl: './our-work.component.html',
  styleUrls: ['./our-work.component.scss']
})
export class OurWorkComponent implements OnInit {

  @ViewChild(MatSidenav)  sidenav: MatSidenav;

  lat: number;
  lon: number;

  places$: Observable<Place[]>;
  places: Place[];
  categories: any;
  lastPlace: any;

  constructor(
    private ms: MapService
  ) { }

  ngOnInit() {
    this.lat = 44.057071;
    this.lon =  -121.315266;
    this.startPlaceSubscription();
  }

  startPlaceSubscription() {
    this.places$ = this.ms.getPlaces();
    this.places$.subscribe((places) => {
      places = this.setDefaultSelection(places);
      this.places = places;
      this.categories = this.transformArray(places, 'category');
      console.log(this.places);
    });
  }

  setDefaultSelection(places) {
    places.map((place) => {
      place.selected = false;
    });
    return places;
  }

  selectPlace(place, e) {
    e.stopPropagation();
    if (this.lastPlace) {
      this.lastPlace.selected = false;
      if (place.id === this.lastPlace.id) {
        place.selected = false;
        this.lastPlace = null;
      } else {
        place.selected = true;
        this.lastPlace = place;
      }
    } else {
      place.selected = true;
      this.lastPlace = place;
    }
  }

  transformArray(array: Array<any>, field) {
    if (array) {
      const groupedObj = array.reduce((prev, cur) => {
        if (!prev[cur[field]]) {
          prev[cur[field]] = [cur];
        } else {
          prev[cur[field]].push(cur);
        }
        return prev;
      }, {});
      return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
    }
    return [];
  }

  isScreenSmall(): boolean {
    return window.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`).matches;
  }

  /*
  openSidenav(place) {
    this.sidenav.opened ? console.log('opened') : this.sidenav.open();
    this.currentPlace = place;
  }
  closeSidenav() {
    this.sidenav.close();
  }
  */

}
