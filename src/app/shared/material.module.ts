import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatTabsModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSidenavModule,
  MatListModule,
  MatGridListModule,
} from '@angular/material';

import { ScrollDispatchModule } from '@angular/cdk/scrolling';

const mat = [
  BrowserAnimationsModule,
  MatTabsModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSidenavModule,
  MatListModule,
  MatGridListModule,
  ScrollDispatchModule
];

@NgModule({
  imports: [ mat ],
  exports: [ mat ]
})
export class MaterialModule { }
